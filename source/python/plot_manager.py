import pickle

class PlotManager:

    def __init__(self):
        self._index = 0
        self._closed = False
        pass

    def is_connection_closed(self):
        return self._closed

    def get_current_samples(self, max_samples=20):
        """Yields one threshold and one curves per time"""
        with open("certified_peaks.pickle", 'rb') as fin:
            peaks = pickle.load(fin)

        threshold_curves = peaks['cursor']
        curve_info = threshold_curves[self._index]
        self._index += 1

        if self._index == len(threshold_curves):
            self._closed = True

        return {self._index - 1: curve_info}

    def real_get_current_samples(self):
        pass