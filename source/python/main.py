import sys
import time
from threading import Thread

from PyQt5.QtWidgets import QApplication

from source.python.editor.MainWindow import MainWindow


def main(args=None):
    application = QApplication(args)
    main_window = MainWindow()
    main_window.show()

    class UpdatePlotThread(Thread):

        def __init__(self):
            super().__init__()
            self.running = None

        def run(self):
            self.running = True
            while not main_window.plot_manager.is_connection_closed() and self.running:
                main_window.update_curves()
                time.sleep(2)

        def stop(self):
            self.running = False
            self.join()

    thread = UpdatePlotThread()
    thread.start()
    application.exec_()
    thread.stop()

if __name__ == '__main__':
    main(sys.argv)