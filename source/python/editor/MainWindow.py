import collections

import numpy as np
import pyqtgraph as pg
from PyQt5.QtWidgets import QMainWindow, QHBoxLayout

from source.python.editor.MainWindowQT import Ui_MainWindow
from source.python.plot_manager import PlotManager


class MainWindowQT(Ui_MainWindow, QMainWindow):
    def __init__(self):
        super().__init__()
        super().setupUi(self)


class MainWindow(MainWindowQT):

    def __init__(self):
        super().__init__()

        pg.setConfigOption('background', 'w')

        # Out access to the model
        self.plot_manager = PlotManager()

        # Our view
        self.plot_window = pg.GraphicsWindow()

        # Initialize all the plots
        self._curve_layout = QHBoxLayout(self.fr_main_plot)
        self._curve_layout.addWidget(self.plot_window)

        self.curve_plot = self.plot_window.addPlot(title='Curves', row=0, col=0)
        self.markers_plot = self.plot_window.addPlot(title='Centroid Markers', row=1, col=0)
        self.histogram_plot = self.plot_window.addPlot(title='Histogram', row=2, col=0)

        # self.curve_plot_item = self.curve_plot.plot()
        self.markers_plot_item = self.markers_plot.plot()
        self.histogram_plot_item = self.histogram_plot.plot()

        # self.histogram_plot.scene().sigMouseMoved.connect(self._on_histogram_mouse_moved)

        self._centroid_to_count = collections.OrderedDict()
        self._centroid_values = []

        self._histogram_label = pg.TextItem('InitialText')
        self.histogram_plot.vb.addItem(self._histogram_label)
        self._histogram_label.hide()

        self._visible_curves = []
        self._hidden_curves = []

        self._visible_regions = []
        self._hidden_regions = []

    def _hide_all_curves(self):
        for curve in self._visible_curves:
            curve.hide()
        self._hidden_curves = self._visible_curves
        self._visible_curves = []

        for region in self._visible_regions:
            region.hide()
        self._hidden_regions = self._visible_regions
        self._visible_regions = []

    def _on_histogram_mouse_moved(self, event):
        # print(self.histogram_plot.boundingRect(), event)
        if self.histogram_plot.sceneBoundingRect().contains(event):
            # We need to find which label to draw
            plot_pos = self.histogram_plot.vb.mapSceneToView(event)
            for centroid, count in self._centroid_to_count.items():
                if abs(plot_pos.x() - centroid) < 100 and abs(plot_pos.y() - count) < 15:
                    # Found it
                    self._histogram_label.setPos(plot_pos.x(), plot_pos.y())
                    self._histogram_label.setText(str(centroid), color='FFFFFF')
                    self._histogram_label.show()
                    return
            self._histogram_label.hide()

    def update_curves(self):
        """Updates all three plot regions"""
        if self.plot_manager.is_connection_closed():
            print('Closed connection')
            return

        # Retrieve the output from the model
        curves = self.plot_manager.get_current_samples()
        peak_positions = []

        for plot in self._visible_curves:
            plot.hide()

        # 1. Plot curves
        self._hide_all_curves()
        for curve_id, curve_info in curves.items():
            _, y_values, peak_position = curve_info

            peak_positions.append(peak_position)

            # Plot curve
            pen = pg.mkPen(color=pg.intColor(curve_id), width=3)
            if self._hidden_curves:
                plot = self._hidden_curves.pop(0)
            else:
                plot = self.curve_plot.plot()
            plot.setData(y=y_values, pen=pen)
            plot.setDownsampling(auto=True, method='peak')
            plot.show()
            self._visible_curves.append(plot)

        # 2. Plot markers and regions
        for peak_position in peak_positions:
            if not len(peak_position) % 2 == 0:
                continue

            for i in range(0, len(peak_position), 2):
                peak_start, peak_end = peak_position[i], peak_position[i + 1]

                # Draw regions for each peak
                if self._hidden_regions:
                    region = self._hidden_regions.pop(0)
                else:
                    region = pg.LinearRegionItem(movable=False)
                region.setZValue(100)
                region.setRegion([peak_start, peak_end])
                region.show()
                self._visible_regions.append(region)
                self.curve_plot.addItem(region, ignoreBounds=True)

                centroid = float(peak_start + (peak_end - peak_start)/2)

                if centroid in self._centroid_to_count:
                    self._centroid_to_count[centroid] += 1
                else:
                    self._centroid_to_count[centroid] = 1
                self._centroid_values.append(centroid)
        self.markers_plot_item.setData(y=self._centroid_values, symbol='o', symbolSize=5)

        # 3. Plot histogram
        y, x = np.histogram(sorted(self._centroid_values), density=False)

        self.histogram_plot_item.setData(x=x,
                                         y=y,
                                         stepMode=True,
                                         fillLevel=0,
                                         brush=(255, 0, 0))
